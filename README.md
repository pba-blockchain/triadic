# TRIADIC #



### What is TRIADIC? ###

**TRIADIC** is a UI that visualizes the **Polyient Games Ecosystem (PGE)**, **Biconomy**, 
and **Avalanche** blockchains (**X-Chain**, **P-Chain**, **C-Chain**) in a single dashboard.